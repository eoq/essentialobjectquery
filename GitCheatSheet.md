# git Cheat Sheet

Set all submodule to track the master branch (necessary only once)

    git submodule foreach --recursive git switch master

Update all submodules to newest branch head

    git submodule foreach --recursive git pull

Commit all changed submodules

    git submodule foreach --recursive "git commit -a -m 'updated submodules to newest version' || true"

Push all changes of all submodules

    git push --recurse-submodules=on-demand




![Essential Object Query Logo](/Figures/EoqLogo.svg)

# EOQ - Essential Object Query

**(INFO: This text is an excerpt from the EOQ User Manual. Please find the full User Manual here: https://gitlab.com/eoq/doc )**

Essential Object Query (EOQ)
Essential Object Query (EOQ) is a language to interact remotely and efficiently with object-oriented models, i.e. domain-specific models. It explicitly supports the search for patterns, as used in model transformation languages. Its motivation is an easy to parse and deterministically behaving query structure, but as high as possible efficiency and flexibility. EOQ’s capabilities and semantics are similar to the Object-Constraint-Language (OCL), but it supports in addition transactional model modifications, change events, and error handling. For instance, 

    GET !Task/name    (textual form)
	
    Get(Cls('Task').Pth('name'))  (API form)

retrieves all values of the attribute “name” of all objects of type “Task” existing in the domain. The return value is a list of strings, e.g.

    ['Flight Control','Stick Input','Security Monitor',...]
   
Concurrent interactions of multiple clients with the same model are supported. EOQ itself is independent of a certain meta-model. The reference implementation is based on models defined within ECORE language from the Eclipse Modeling Framework (EMF). EOQ implementations are available for [Python](https://gitlab.com/eoq/py/pyeoq) and [Javascript](https://gitlab.com/eoq/js/jseoq). 


## Basic Principle
EOQ’s main purpose are efficient interactions of a **client** with a domain-specific model via a **domain**. The domain can be on the same computer or remote. The domain is the sole interface to the model and the client is an arbitrary program working with model data. There are two kinds of interactions as depicted in Figure 1: 

(1) Normal information retrieval and model modifications works with stateless a **command** (CMD) issued from the client to the domain and a corresponding **result** (RES) returned from the domain to the client. Commands are always executed as ACID **transactions**. The sole interface for commands is the **Do** function of the domain. In addition, it is possible to listen to changes on the domain by events, e.g. the modification of an objects attribute by a third party. The structure of commands, results, and events is described in the following. The major building block of all commands are queries (e.g. Essential Object Queries). Queries allow for accessing, filtering, and searching information from a domain-specific model.

(2) Domain interactions can cause **events**, e.g. model modification actions. Every client can listen to events such that he, for instance, gets immediately informed on model changes. Events are triggered by the domain and distributed to registered clients.

EOQ assumes domain-specific models to be formally defined by a meta-model. It adapts the ECORE model of the [Eclipse Modeling Framework (EMF)](https://www.eclipse.org/modeling/emf/), which is the references implementation of the meta-modelling standard [Meta Object Facility (MOF)](https://www.omg.org/spec/MOF/About-MOF/) of the Object Management Group. The EOQ domain hosts both the meta-model in ECORE format and the domain-specific alias user model and commands and queries allow access to both. Commands allow information retrieval and modification in a transaction-based manner. Queries simplify the access to the data by allowing fast and object-oriented navigation, searching, and filtering of model information, with operations similar, but not identical to the [Object Constraint Language (OCL)](https://www.omg.org/spec/OCL). An additional feature are **actions**, which allow repeatable complex operations on domains-specific data.

![Basic principle of using EOQ to interact with domain-specific model data](/Figures/EoqBasicPrinciple.svg)

_Figure 1: Basic principle of using EOQ to interact with domain-specific model data_

##	Illustrative Example

A simple and complete graphical example of how EOQ is used to interact with the elements of a domain-specific model is given in Figure 2. The example shows a meta-model of an avionics computing architecture composed of _Device_ and _DeviceType_ classes, both exhibit a name attribute by inheriting from _NamedElement_. Based on this meta-model the depicted user model (domain-specific model) was created. The user model device three _Device_ objects and two _DeviceType_ objects. An EOQ GET command is used to figure out all devices of type with the name “CPM”. The result is the _Device_ objects “CPM 1” and “CPM 2”. 

![Educational example of model and the effect of EOQ get and set commands and queries](/Figures/EoqIlustrativeExample.svg) 

_Figure 2: Educational example of model and the effect of EOQ get and set commands and queries_

Denote that EOQ will not return objects, but unique identifiers (EOQ ID) on object starting with a “#”. In a second get command it is looked for all _DeviceType_ objects with the name “RDC”, but only the first that fulfils this criterion. This returns object ID #3. In a third command, a set command, the _type_ relation of the _Device_ objects returned by command 1 are set to the _DeviceType_ object returned by command 2.

If executing command 1 of the example above in textual EOQ the interaction would look as given in Listing 1. The client issues a command (CMD) to the domain, which is a GET command defined by a query looking for all objects of class _Device_ and selecting ({…}) only those whose type’s name equals “CPM”. The result contains a status (OKY), a transaction ID (1), change ID (0), and actual value ([#1,#2]). The returned objects are returned by EOQ as Hash-IDs (EOQ ID) in order to make them serializable.   

    CMD: GET !Device{/type/name='CPM'}
    RES: RES OKY 1 0 [#1,#2]
	
_Listing 1: Command based (textual) interaction with EOQ for the educational example_

This and all further examples in the documentation are based on the [Open Avionics Architecture Model (OAAM)](http://www.oaam.de) and its example architecture model _MinimalFlightControl.oaam_. OAAM is only used as an example. It is not mandatory and has no connection to EOQ. Basically, any EMF-based meta-model(s) and model(s) can be used with EOQ. If object references are used in the examples (e.g. #20 ) those are placeholders, since the IDs might change depending on the access order of objects. All examples can be reproduced using PyEoq implementation and the included example scripts.

## Getting Started Example

_(This example is available as py/pyeoq/Examples/WhyWarehouseTextual.py)_

Imagine having the following hierarchically organized domain-specific model of you warehouse? 

                                                          _______________
     ___________              _______________            |    Article    |
    | Warehouse | categories |   Category    |  articles |---------------|
	|-----------|<>--------->|---------------|<>-------->| name : string |
	|           |           *| name : string |          *| price : real  |
	|___________|        o-->|_______________|<>-o       | sells : int   |
                         |  *                    |       |_______________|
                         o-----------------------o
                               categories

Would you like to get a list of articles?

    GET !Article

Would you like to know which one is sold less than 3-times?

    GET !Article{/sells<3}

You might want to reduce their price by 10%?

    SET !Article{/sells<3} 'price' !Article{/sells<3}/price&MUL0.9

Would you like to see a list of the names of the categories of the badly selling articles sorted  ascendingly?

    GET !Article{/sells<3}@PARENT_[]\name:SORTASC
	

##	Getting Started
The best way getting started theoretically is reading this documentation. The best way getting started practically is by looking into one of the implementations and the included examples. 

Repository URL:  https://gitlab.com/eoq/essentialobjectquery 

In order to try EOQ directly without any programming you can use the [EOQ Web Server](https://gitlab.com/eoq/tools/pyeoqwebserver) and the [EOQ Developer Interface](https://gitlab.com/eoq/tools/jseoqdeveloperinterface) from the [EOQ tools repository](https://gitlab.com/eoq/tools/eoqtools): 

1)	Start  essentialobjectquery/tools/pyeoqwebserver/Test/StartPyEoq2WebServer.bat
2)	Open essentialobjectquery/tools/jseoqdeveloperinterface/jseoqdeveloperinterface.html

Use the EOQ Developer interface to enter EOQ commands manually and view the results as depicted in Figure 4.
 
![Try EOQ directly using the EOQ Web Server and the EOQ Developer Interface from the tools repository](/Figures/SetupToTryEoqOutOfTheBox.svg)  
 
_Figure 4: Try EOQ directly using the EOQ Web Server and the EOQ Developer Interface from the tools repository_

## List of EOQ Commands
PyEOQ supports the follwing list of EOQ commands. For more details see [here](https://gitlab.com/eoq/doc).

Command  | Result  | 3-letter identifier  | API function (programming lang.)
--- | --- | --- | --- 
Get  | value  | GET  | Get(query)
Set  | [rtarget, rfeature, rvalue]  | SET  | Set(target,feature,value)
Add  | [rtarget, rfeature, rvalue]  | ADD  | Add(target,feature,value)
Remove  | [rtarget, rfeature, rvalue]  | REM  | Rem(target,feature,value)
Move  | [rtarget, rfeature, rvalue]  | MOV  | Mov(target,newIndex)
Clone  | clone  | CLO  | Clo(target,mode)
Create   | value  | CRT  | Clo(class,n,([...]))
Create by Name  | value  | CRN  | Crn(packageName,className,n,([...]))
Querify  | query  | QRF  | Qrf(target)
Hello  | sessionId  | HEL  | Hel(user,password)
Goodbye  | true  | GBY  | Gby(sessionId)
Session  | true  | SES  | Ses(sessionId)
Get All Meta-models  | [mm1, mm2, ...]  | GMM  | Gmm()
Register Meta-model  | true  | RMM  | Rmm(package)
Unregister Meta-model  | true  | UMM  | Umm(package)
Status  | changeId  | STS  | Sts()
Changes  | [chg1,...]  | CHG  | Chg(changeId,n)
Observe Event  | True  | OBS  | Obs(eventType,eventKey)
Unobserve Event  | True  | UBS  | Ubs(eventType,eventKey)
Get All Actions  | [action1,...]  | GAA  | Gaa()
Call Action  | [callId, status, value, outputs]  | CAL  | Cal(name(,[arg1,...],[opt1,...]]))
Async. Action Call  | callId  | ASC  | Asc(name(,[arg1,...],[opt1,...]))
Abort Call  | success  | ABC  | Abc(callId)
Call status  | (not implemented)  | CST  | Cst(callId)
Compound  | [result1,...]  | CMP  | Cmp(). (trailing commands)


## List of EOQ Queries Segments
PyEOQ supports the follwing set of EOQ query segments. For more details see [here](https://gitlab.com/eoq/doc).

Segment  | Type  | 3-letter identifier  | Symbol (textual repr.) | API function (programming lang.)
--- | --- | --- | --- | ---
Object  | Context-less  | OBJ  | #  | Obj(objref)
History  | Context-less  | HIS  | $  | His(id)
Path  | Context-only Element-wise   | PTH  | /  | Pth(name)
Class  | Context-only Element-wise   | CLS  | !  | Cls(className)
Instance of  | Context-only Element-wise   | INO  | ?  | Ino(className)
Meta Operation  | Context-only Element-wise (might vary depending on op.)  | MET  | @  | Met(operationName(,[arg,...]))
Not  | Context-only Element-wise  | NOT  | &NOT  | Not()
Terminate  | Special Operation  | TRM  | &TRM  | Trm(condition,default)
Index  | Context-only List-operation   | IDX  | :  | Idx(n)
Selector  | Context-only List-operation   | SEL  | {  | Sel(selectionCriteria)
Array  | Context-only List-operation   | ARR  | [  | Arr([elem1,...])
Zip structures  | Context-only List-operation   | ZIP  | &ZIP  | Zip([elem1,...])
Query / Subquery  | Context-only List-operation   | QRY  | (  | Qry((root))
Contains Any  | Context-only List-operation  | ANY  | &ANY  | Any(selectionCriteria)
Contains All  | Context-only List-operation  | ALL  | &ALL  | All(selectionCriteria)
Is Equal  | Context-vs-args Element-wise  | EQU  | =  | Equ(operand2)
Equals any  | Context-vs-args Element-wise  | EQA  | &EQA  | Eqa([op1,op2,...])
Is Not Equal  | Context-vs-args Element-wise  | NEQ  | ~  | Neq(operand2)
Is Greater  | Context-vs-args Element-wise  | GRE  | >  | Gre(operand2)
Is Less  | Context-vs-args Element-wise  | LES  | <  | Les(operand2)
Includes regular expression | Context-vs-args Element-wise  | RGX  | &RGX  | Rgx(regex)
Addition / OR  | Context-vs-args Element-wise  | ADD  | &ADD  | Add(operand2)
Subtraction /XOR  | Context-vs-args Element-wise  | SUB  | &SUB  | Sub(operand2)
Multiplication / AND  | Context-vs-args Element-wise  | MUL  | &MUL  | Mul(operand2)
Division / NAND  | Context-vs-args Element-wise  | DIV  | &DIV  | Div(operand2)
OR  | Context-vs-args Element-wise  | ORR  | &ORR  | Orr(operand2)
XOR  | Context-vs-args Element-wise  | XOR  | &XOR  | Xor(operand2)
AND  | Context-vs-args Element-wise  | AND  | &AND  | And(operand2)
NAND  | Context-vs-args Element-wise  | NAD  | &NAD  | Nad(operand2)
Cross product  | Context-vs-args List-operation  | CSP  | %  | Csp(operand2)
Intersection  | Context-vs-args List-operation  | ITS  | ^  | Its(operand2)
Set difference   | Context-vs-args List-operation  | DIF  | \  | Dif(operand2)
Union  | Context-vs-args List-operation  | UNI  | _  | Uni(operand2)
Concatenation  | Context-vs-args List-operation  | CON  | \|  | Con(operand2)



